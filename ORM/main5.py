from typing import List, Optional

import uvicorn
from fastapi import FastAPI, HTTPException
from sqlmodel import Field, SQLModel, select, create_engine, Session
from starlette import status

connect_args = {"check_same_thread": False}
engine = create_engine("sqlite:///schedule.db", echo=True,
                       connect_args=connect_args)

app = FastAPI()


class PERSONBase(SQLModel):
    person: str
    university: str
    course: int
    grup: int
    item_number: int


class PERSON(PERSONBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    person: Optional[str] = Field(default=None, primary_key=False)
    university: Optional[str] = Field(default=None, primary_key=False)
    course: Optional[int] = Field(default=None, primary_key=False)
    grup: Optional[int] = Field(default=None, primary_key=False)
    item_number: Optional[int] = Field(default=None, primary_key=False)


class PERSONToGet(PERSONBase):
    id: int
    person: str
    university: str
    course: int
    grup: int
    item_number: int


class PERSONToCreate(PERSONBase):
    person: str
    university: str
    course: int
    grup: int
    item_number: int


class PERSONToUpdate(PERSONBase):

    pass


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()


@app.get("/PERSON", response_model=List[PERSON])  # 127.0.0.1:8080/PERSON
def get_PERSON() -> List[PERSON]:
    with Session(engine) as session:
        person: List[PERSON] = session.exec(select(PERSON)).all()
    return person


@app.post("/PERSON", status_code=201)
def add_PERSON(user: PERSONToCreate):
    with Session(engine) as session:
        db_schedule = PERSON.from_orm(user)
        session.add(db_schedule)
        session.commit()
        session.refresh(db_schedule)
        return db_schedule


@app.patch("/PERSON/{PERSON_id}", status_code=status.HTTP_200_OK)
def update_PERSON(PERSON_id: int, person: PERSONToUpdate):
    with Session(engine) as session:
        db_schedule = session.get(PERSON, PERSON_id)
        if not db_schedule:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="PERSON not found")
        db_schedule.person = person.person
        db_schedule.university = person.university
        db_schedule.course = person.course
        db_schedule.grup = person.grup
        db_schedule.item_number = person.item_number
        session.add(db_schedule)
        session.commit()
        session.refresh(db_schedule)
        return db_schedule


@app.delete("/PERSON/{PERSON_id}")
def delete_PERSON(PERSON_id: int):
    with Session(engine) as session:
        person = session.get(PERSON, PERSON_id)
        if not person:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="PERSON not found")
        session.delete(person)
        session.commit()
        return person


@app.get("/PERSON/{PERSON_id}")
def get_PERSON(PERSON_id: int) -> Optional[PERSON]:
    with Session(engine) as session:
        person = session.get(PERSON, PERSON_id)
        if not person:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="PERSON not found")
        return person


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)  # 127.0.0.1:8080
