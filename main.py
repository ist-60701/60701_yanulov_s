import sqlite3

import uvicorn
from fastapi import FastAPI

conn = sqlite3.connect(r"schedule.db", check_same_thread=False)
app = FastAPI()

@app.get("/PERSON")
def get_PERSON():
    cursor = conn.execute("SELECT * FROM PERSON")
    result = [
        {
            "id": PERSON[0], "person": PERSON[1], "university": PERSON[2], "course": PERSON[3], "grup": PERSON[4], "item_number": PERSON[5]
        } for PERSON in cursor.fetchall()
    ]
    return result

@app.get("/schedule")
def get_schedule():
    cursor = conn.execute("SELECT * FROM schedule;")
    result = [
        {
            "item_id": schedule[0], "item": schedule[1]
        } for sc_contacts in cursor.fetchall()
    ]
    return result

if __name__ == "__main__":
    cur = conn.cursor()
    uvicorn.run(app, host="127.0.0.1", port=8080)
