import sqlite3

import uvicorn
from fastapi import FastAPI
from fastapi import FastAPI, Request, status, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

conn = sqlite3.connect(
    r"schedule.db", check_same_thread=False)
app = FastAPI()


templates = Jinja2Templates(directory="templates")


@app.get("/")
def info(request: Request):
    coursor = str(
        "Hi, information about users and their items , add to the address link: '/PERSON, /schedule ")
    return templates.TemplateResponse("hello.html", {"coursor": coursor, "request": request})


@app.get("/PERSON")
def get_PERSON(request: Request):
    cursor = conn.execute("SELECT * FROM PERSON")
    result = [
        {
            "id": PERSON[0], "person": PERSON[1], "university": PERSON[2], "course": PERSON[3], "grup": PERSON[4], "item_number": PERSON[5]
        } for PERSON in cursor.fetchall()
    ]
    return templates.TemplateResponse("PERSON.html", {"result": result, "request": request})


@app.get("/schedule")
def get_schedule(request: Request):
    cursor = conn.execute("SELECT * FROM schedule;")
    result = [
        {
            "item_id": schedule[0], "item": schedule[1]
        } for schedule in cursor.fetchall()
    ]
    return templates.TemplateResponse("schedule.html", {"result": result, "request": request})


@app.post("/PERSON")
def fetch_data(id: int, person: str, university: str, course: int, grup: int, item_number: int):
    insert_query = f"INSERT INTO  User(id, person, university, course, grup, item_number) VALUES({id}, {person}, {university}, {course}, {grup}, {item_number})"
    cursor = conn.execute(insert_query)
    results = conn.commit()


@app.delete("/PERSON/{id}")
def delete_user(id: int):
    cursor = conn.execute(f"SELECT * FROM User WHERE User.id={id};")
    user = cursor.fetchone()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="user not found"
        )
    conn.execute(f"DELETE FROM User WHERE id={id}")
    conn.commit()


@app.patch("/PERSON/{id}", status_code=status.HTTP_200_OK)
def update_user(id: int, person: str, university: str, course: int, grup: int, item_number: int):
    cursor = conn.execute(f"SELECT * FROM User WHERE user.id={id};")
    query_result = cursor.fetchone()
    if not query_result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="user not found")
    conn.execute(
        f"UPDATE User SET person = ('{person}'), university = ('{university}'), course = ('{course}'), grup = ('{grup}'), item_number = ('{item_number}') WHERE id={id}")
    conn.commit()


if __name__ == "__main__":

    uvicorn.run(app, host="127.0.0.1", port=8080)
